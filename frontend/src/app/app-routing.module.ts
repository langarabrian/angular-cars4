import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ThingsComponent }      from './things/things.component';

const routes: Routes = [
  { path: 'things', component: ThingsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
